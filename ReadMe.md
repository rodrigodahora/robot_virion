## ReadMe para Robocode_VírioN

Finalizado 28-out-2020 por Rodrigo N. Da Hora

Robocode Home Page:
[https://robocode.sourceforge.io/](https://robocode.sourceforge.io/)

### TABELA DE INSTRUÇÕES

1.  [Robô VírioN](#VírioN_ROBOT)
2.  [Considerações.](#Pontos fortes e Pontos fracos)
3.  [Considerações Finais](#Considerações finais)


### 1. Robô VírioN?

Foi criado um robô simples, utilizando a linguagem de programação Java.
A ideia seria a construção de um robô que conseguisse desviar de projeteis,
mantendo o seu eixo em rotação e movimentos constantes. Porém, em cada esquiva
ele exerce o poder de fogo, escaneando tudo ao seu redor, com scanners de 360º 
para não deixar passar qualquer inimigo. Uma das funcionalidades inseridas foi
a que, juntamente ao scan, o robô verifica uma distância de "perigo" e tende a 
se afastar do inimigo, assim também verifica uma distância não tão favorável para
área de ataque. Consta também o evento no qual ele abusa da colisão com um inimigo
e faz um disparo com força máxima, apenas nessas condições, onde a taxa de erro
do projétil é muito baixa. Assim economizando e aplicando uma maior energia do robô
apenas quando realmente seja eficaz. Foi feito uso de estruturas de repetição como
o 'for' e 'while' para a movimentação automática do robô, tendo em foco o desviar
de projéteis. Utilizado também recursos de estrutras condicionais como 'if' 
e 'else if' para auxiliar tanto na segurança como no ataque do robô. 

**Considerações. pontos fortes e fracos**

-Pontos Fortes-

O uso do movimento "aleatório" mesmo sendo padronizado, é de extrema importância
para evitar ser atingido por vários projéteis ao mesmo tempo. Tendo em mente 
que o processo é realizado por turnos, no caso do robô VírioN, é uma tentativa de
otimização do mesmo, para que não só esquive, mas que tenha tempo hábil para ataque.

Escanear um ou mais robô's na proximidade e fazer disso uma prioridade para locomoção
é um salva vida. Ele interrompe qualquer outro movimento, para que seja executado
a fuga e o ataque consecutivos. Sendo assim, ele utiliza o fator tempo como aliado
e se reposiciona para um próximo ataque/fuga.

Utilizar a colisão com o inimigo como uma disposição para um ataque mais efetivo
é sem dúvidas um dos melhores eventos. O robô está programado para executar um disparo
com força máxima, quando ocorrer uma colisão. Da mesma forma que consecutivamente
ele exerce uma fuga, o scan, verificando que o robô inimigo está muito próximo
tende a fazê-lo se afastar da zona de perigo. 

-Pontos Fracos-

Um dos pontos fracos no robô VírioN é a indisposição da verificação angular do
inimigo que passar em seu scan. Sabe-se a distância, mas não a localização dele no 
mapa. Sendo possível verificar a localização e angulação, o scan do robô é em 
formato de cône, sendo assim seria possível prever para onde o robô inimigo estaria
se movimentando, quando saísse da visão do scan, dependendo para qual ângulo.
Considerando que, o scan tem uma validação muito rápida, a "mira" poderia de alguma 
forma ser travada no inimígo. 

A falta de efetividade nas colisões, tanto com o inimigo quanto com a parede, está
no próprio eixo do robô. Não foi implementado uma forma de saber em qual ângulo o
robô VírioN está disposto em relação ao robô inimigo ou à uma parede quando colidem. 
Logo, a reação é quase que espontânea, torna-se possível que, passivo de sabedoria
sobre angulos, o próprio robô siga um caminho indesejado.

O maior ponto fraco do robô VírioN é não realizar os movimentos e disparos simutaneamente
e sim consecutivamente. Sendo assim uma falta de otimização dada a uma escassez 
fundamental de conhecimento. É algo de vital importância para uma competição que, 
o robô seja totalmente automatizado, sem brechas de chances de ser atingido pelo
inimigo. Ou seja, a cada movimento feito, existe o intervalo de no mínimo um segundo
para que o robô possa ser atingido. Pensando nisso, uma forma de tentar evitar,
foi a movimentação característica do próprio robô.


### Considerações Finais

Minha maior satisfação na construção do robô foi fazer com que tudo que consegui
construir fosse realmente efetivo. Apesar de ter a consciência de que caberia a
mim fazer muito mais, afinal, tenho em mente o que poderia ser ainda melhor para 
o mesmo. Me falta conhecimento, esse é o objetivo maior em tentar a vaga. O maior
desafio, sem dúvidas, é ter a noção de que poderia ser implementado algo maior, 
vizualisar o que poderia ser feito e não saber fazer. Embora existam vários exemplos
no vasto mundo que é a internet. Foi preferível a mim, me manter no que realmente 
consigo fazer, a tentar absorver algo novo e maior em pouco tempo e acabar errando.
Agradeço à Solutis e a mim mesmo pela oportunidade e tentativa, o projeto está 
sendo incrível.