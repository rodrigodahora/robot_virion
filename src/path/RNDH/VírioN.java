package RNDH;
import robocode.Robot;
import java.awt.Color;
import robocode.*;
//import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html
	
/**
 * VírioN - a robot by (Rodrigo N. Da Hora)
 */
public class VírioN extends Robot
{
		
	public void run() {
		
		setBodyColor(Color.PINK); //Colocar cor rosa no robô
		setBulletColor(Color.RED); //Colocar cor da bala vermelha
		setRadarColor(Color.CYAN); //Colocar cor do radar ciano
		setScanColor(Color.ORANGE); //Colocar cor do Scan laranja
	
		boolean curva = true; //Variavel curva setada como verdadeira
		
		// Looping principal do Robô
		while(true){
		
		if(curva == true){ //Situação de validação da variável sendo ela verdadeira
			//Código a ser executado 
			for(int direita=0; direita<2;direita++){ //Looping para movimento à direita do próprio eixo, 3 repetições do movimento se não houver interrupção
			ahead(80);	//Seguir à frente		
			turnGunRight(360); //Canhão girar para a direita executando 360º
			turnRight(80); //Robô virando 80º à direita do próprio eixo.
		
			}
		
			for(int esquerda=5; esquerda>2; esquerda--){ //Looping para movimento à esquerda do próprio eixo, 3 repetições do movimento se não houver interrupção
														//Valor da variável é decrementado em cada turno
			turnGunLeft(360); //Canhão girar para a esquerda executando 360º
			ahead(80);
			turnLeft(80);
			back(50);

			}
		curva = false; //Setar o valor curva como falso impede que ele entre no if na próxima rotação e entre direto no else
		
		}else{ //Executa o código e faz com que o valor verdadeiro seja novamente setado na variável curva
		
			for(int esquiva=0; esquiva<3;esquiva++){
			ahead(200);
			turnGunRight(360);
			turnRight(90);
			back(30);
			
			curva = true; //Levando a variável curva a se tornar verdadeira no turno que o else for executado
		
			}
		}
	}
}

	 
	public void onScannedRobot(ScannedRobotEvent e) { //No scanner do robô
		if(e.getDistance() >= 150){ //Vai verificar se o robô inimigo está a no mínimo 150 pixel's de distância.
									//Se sim, vai atirar e andar 50 pixel's à frente 
			fire(3);
			ahead(50);
			

		}else if(e.getDistance() < 100){ //Utilização do else if para dar mais uma condição verdade ao scan 
								//Se estiver a 100 pixel's de distância, vai atirar e andar 80 pixel's para trás
			fire(2);
			back(80);
					
			
		}else{ //Se nenhum dos dois outros casos acontecer ele simplesmente da um scan de 360º e atira se encontrar.
			turnGunRight(360);
			fire(2);

		}
		
	}

	
	public void onHitByBullet(HitByBulletEvent e) { //Ao evento que o meu robô é atingido por um progétil.
	//Ele tenta uma esquiva de um possível proximo disparo, executando o movimento de virar 45º à esquerda do próprio eixo e retornar para trás em 80 pixel's
		turnLeft(45);
		back(80);	
		
	}
	
	public void onHitRobot (HitRobotEvent e) {
		//Quando ocorre colisão com outro robô.
			back(10); // Mover-se para trás em 10 pixels
			turnGunRight(360);
			fire(Rules.MAX_BULLET_POWER); //Aproveitar a proximidade para executar um disparo com força máxima, margem de erro muito pequena.

	}
	
	public void onHitWall(HitWallEvent e) {
		// Quando ocorre colisão com as paredes do mapa
		// Para não ocorrer do robô ficar preso a uma parede, assumo sua colisão em um valor aproximado entre 0 e 80º, sendo que ele não colide de lado
		// A ação do robô é se locomover 80 pixel's para trás e girar no eixo de 120º
		ahead(80);
		turnRight(120);
		}	
	}
